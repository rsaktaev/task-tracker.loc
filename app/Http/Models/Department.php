<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $fillable = [
        'name'
    ];

    public function users() {
        return $this->hasMany('users');
    }

    public function tasks() {
        return $this->hasMany('tasks');
    }
}
