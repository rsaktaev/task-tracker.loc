<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class AttachedTaskPhoto extends Model
{
    //
    protected $fillable = [
        'link'
    ];

}
