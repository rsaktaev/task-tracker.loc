<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = [
        'title', 'description', 'is_completed', 'deadline'
    ];

    public function creator() {
        return $this->belongsTo('App\Http\Models\User', 'creator_id');
    }

    public function executor() {
        return $this->belongsTo('App\Http\Models\User', 'executor_id');
    }

    public function department() {
        return $this->belongsTo('App\Http\Models\Department');
    }

    public function photos() {
        return $this->hasMany('App\Http\Models\AttachedTaskPhoto');
    }

    //картинки о выполнении
    public function scopeCompletedPhotos($query) {
        return $this->photos()->where('is_completed', '=', 1);
    }

    //картинки к задаче
    public function scopeTaskPhotos($query) {
        return $this->photos()->where('is_completed', '=', 0);
    }
}
