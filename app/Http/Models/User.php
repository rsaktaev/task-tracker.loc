<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'patronymic', 'email', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function executedTasks() {
        return $this->hasMany('App\Http\Models\Task', 'executor_id');
    }

    public function createdTasks() {
        return $this->hasMany('App\Http\Models\Task', 'creator_id');
    }

    public function department() {
        return $this->belongsTo('App\Http\Models\Department');
    }
}
