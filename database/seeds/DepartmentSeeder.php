<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('departments')->insert([
            'name' => 'Отдел контроля'
        ]);

        DB::table('departments')->insert([
            'name' => 'Охрана',
        ]);

        DB::table('departments')->insert([
            'name' => 'Отдел проверки качества',
        ]);

        DB::table('departments')->insert([
            'name' => 'Клининговый отдел',
        ]);
    }
}
