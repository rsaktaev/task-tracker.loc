<?php

use App\Http\Models\Department;
use App\Http\Models\Task;
use App\Http\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = new User([
            'name' => 'Вячеслав',
            'surname' => 'Гордеев',
            'patronymic' => 'Михайлович',
            'email' => 'vyacheslav.gordeev@gmail.com',
            'phone' => '+7 960 42 83',
            'password' => bcrypt('A2design_gordeev'),
            'is_admin' => true
        ]);

        $department = Department::where('name', '=', 'Отдел проверки качества')->first();
        $admin->department()->associate($department);
        $admin->save();
    }
}
