<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachedTaskPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attached_task_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('link')->nullable();
            $table->boolean('is_completed');        //true - если фото к выполненной задаче, false - если к невыполненной

            $table->unsignedInteger('task_id');
            $table->foreign(['task_id'])->on('tasks')->references('id')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attached_task_photos');
    }
}
