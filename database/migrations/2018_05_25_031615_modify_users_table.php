<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('surname')->after('name');
            $table->string('patronymic')->after('surname');
            $table->boolean('is_admin');
            $table->string('phone')->after('email');
            $table->unsignedInteger('department_id');

            $table->foreign('department_id')->on('departments')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign(['department_id']);
        });

        Schema::table('users', function (Blueprint $table) {
            //

            $table->dropColumn('surname');
            $table->dropColumn('patronymic');
            $table->dropColumn('is_admin');
            $table->dropColumn('phone');
            $table->dropColumn('department_id');

        });
    }
}
