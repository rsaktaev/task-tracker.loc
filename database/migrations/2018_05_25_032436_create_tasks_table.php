<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();        //описание задачи
            $table->text('complete_description')->nullable();           //дополнительная информация после выполнения

            $table->unsignedInteger('department_id');
            $table->unsignedInteger('creator_id');
            $table->unsignedInteger('executor_id');

            $table->boolean('is_completed')->default(false);     //true - выполнено, false - не выполнено

            $defaultDate = (new DateTime())->add(new DateInterval('P1D'));

            $table->dateTime('deadline')->default($defaultDate->format('Y-m-d h:i:s'));

            $table->foreign('department_id')->on('departments')->references('id')->onDelete('cascade');
            $table->foreign('creator_id')->on('users')->references('id')->onDelete('cascade');
            $table->foreign('executor_id')->on('users')->references('id')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
